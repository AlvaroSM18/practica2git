package practica2;

import java.util.List;

public class Histogram {

//Modificación para que la clase sea genérica
    private final List<Integer> data;

    public Histogram(List<Integer> data) {
        this.data = data;
        // Modificación del Constructor
    }

    public List<Integer> getData() {
        return data;
        // Modificación del Getter
    }
}
